
/** @type HTMLVideoElement */
const video = document.getElementById('video');
const c1 = document.getElementById('c1');
const ctx1 = c1.getContext('2d');
const c2 = document.getElementById('c2');
const ctx2 = c2.getContext('2d');
let width = 0
let height = 0

function computeFrame() {
    ctx1.drawImage(video, 0, 0, width, height);
    let frame = ctx1.getImageData(0, 0, width, height);
    let l = frame.data.length / 4;
    for (let i = 0; i < l; i++) {
        let r = frame.data[i * 4 + 0];
        let g = frame.data[i * 4 + 1];
        let b = frame.data[i * 4 + 2];
        if (g > 100 && r > 100 && b < 43)
            frame.data[i * 4 + 3] = 0;
    }
    ctx2.putImageData(frame, 0, 0);
    return;
}

function play() {
    if (video.paused || video.ended) {
        return
    }
    computeFrame()
    requestAnimationFrame(play)
}



video.onplay = function () {
    width = video.videoWidth
    height = video.videoHeight
    play()
}