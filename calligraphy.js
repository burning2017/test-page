// @ts-check
/**
 * @typedef {{x:number,y:number}} Point
 */

/** @type CanvasRenderingContext2D  */
let context
/** @type HTMLCanvasElement */
let canvas
let canvasWidth
let canvasHeight
//判断是否按下鼠标
let isMouseDown = false
//记录上一次鼠标所在位置
/** @type Point */
let lastLoc = { x: 0, y: 0 }
let lasTimeStamp = 0
let lastLineWidth = -1
const strokeColor = 'black'
const num = 1
//笔画速度越快，笔越细，反之越粗！
const maxLineWidth = 20
const minLineWidth = 1
const maxLineSpeed = 2
const minLineSpeed = 0.1

function init() {
  canvasWidth = 300
  canvasHeight = canvasWidth * num
  canvas = document.querySelector('#canvas')
  context = canvas.getContext('2d')
  canvas.width = canvasWidth
  canvas.height = canvasHeight
  drawBackGround()
  //鼠标按下
  canvas.onmousedown = function (e) {
    //阻止默认事件响应
    e.preventDefault()
    beginStock({ x: e.clientX, y: e.clientY })
  }
  //鼠标松开
  canvas.onmouseup = function (e) {
    e.preventDefault()
    endStock()
    //console.log("mouseup");
  }
  //鼠标移出指定对象时发生
  canvas.onmouseout = function (e) {
    e.preventDefault()
    endStock()
  }
  //鼠标移动过程中
  canvas.onmousemove = function (e) {
    e.preventDefault()
    if (isMouseDown) {
      moveStock({ x: e.clientX, y: e.clientY })
    }
  }
  //移动端（触碰相关的事件）
  canvas.addEventListener('touchstart', function (e) {
    e.preventDefault()
    //触碰事件，也可能是多点触碰，就是第一个
    let touch = e.touches[0]
    beginStock({ x: touch.pageX, y: touch.pageY })
  })
  canvas.addEventListener('touchmove', function (e) {
    e.preventDefault()
    if (isMouseDown) {
      //console.log("mousemove");
      let touch = e.touches[0]
      moveStock({ x: touch.pageX, y: touch.pageY })
    }
  })
  canvas.addEventListener('touchend', function (e) {
    e.preventDefault()
    endStock()
  })
}

//设置背景
function drawBackGround() {
  context.fillStyle = '#ccc'
  context.fillRect(0, 0, canvasWidth, canvasHeight)
  // TODO 框
  context.setLineDash([5, 10])
  for (let i = 0; i < num; i++) {
    context.moveTo(10, 10 + i * 300)
    context.strokeRect(10, 10, 280, 280)
    context.lineTo(290, 290)
    context.moveTo(150,10)
    context.lineTo(150, 280)
    context.moveTo(290, 10)
    context.lineTo(10, 290)
    context.moveTo(10, 150)
    context.lineTo(290, 150)
    context.stroke()
  }
  context.setLineDash([])
}

//屏幕坐标点转化为canvas画布坐标,定位canvas画布上的坐标
function windowToCanvas(x, y) {
  //包含canvas距离画布的上和左边距
  const rect = canvas.getBoundingClientRect()
  return { x: Math.round(x - rect.left), y: Math.round(y - rect.top) }
}

/**
 * @param {Point} point
 */
function beginStock(point) {
  isMouseDown = true
  lastLoc = windowToCanvas(
    point.x,
    point.y -
      Math.max(document.body.scrollTop, document.documentElement.scrollTop)
  )
  lasTimeStamp = new Date().getTime()
}

function endStock() {
  isMouseDown = false
}
//通过两点计算出两点之间距离
function calDistance(loc1, loc2) {
  return Math.sqrt(
    (loc1.x - loc2.x) * (loc1.x - loc2.x) +
      (loc1.y - loc2.y) * (loc1.y - loc2.y)
  )
}

function CalClientWidth(t, s) {
  const v = s / t
  let ResultLineWidth
  //处理速度很慢和很快的情况
  if (v <= minLineSpeed) ResultLineWidth = maxLineWidth
  else if (v >= maxLineSpeed) ResultLineWidth = minLineWidth
  else
    ResultLineWidth =
      maxLineWidth -
      ((v - minLineSpeed) / (maxLineSpeed - minLineSpeed)) *
        (maxLineWidth - minLineWidth)
  if (lastLineWidth == -1) return ResultLineWidth
  return (lastLineWidth * 2) / 3 + (ResultLineWidth * 1) / 3
}

function moveStock(point) {
  let curLoc = windowToCanvas(
    point.x,
    point.y -
      Math.max(document.body.scrollTop, document.documentElement.scrollTop)
  )
  let curTimeStamp = new Date().getTime()

  let s = calDistance(curLoc, lastLoc)
  let t = curTimeStamp - lasTimeStamp

  let lineWidth = CalClientWidth(t, s)

  //具体的绘制，鼠标按下之后
  context.beginPath()
  context.moveTo(lastLoc.x, lastLoc.y)
  context.lineTo(curLoc.x, curLoc.y)
  context.strokeStyle = strokeColor
  context.lineWidth = lineWidth
  //设置线条的帽子，是线条平滑
  context.lineCap = 'round'
  context.lineJoin = 'round'
  context.stroke()
  lastLoc = curLoc
  lasTimeStamp = curTimeStamp
  lastLineWidth = lineWidth
}

function downloadImage() {
  let image = canvas.toDataURL('image/png')
  let linkElement = document.createElement('a')
  linkElement.setAttribute('href', image)
  linkElement.setAttribute('downLoad', '春联')
  linkElement.click()
  drawBackGround()
}

function onReset() {
  drawBackGround()
}
window.onload = init
